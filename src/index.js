const _validateItems = require('./actions/_validateItems')
const _getTemplateConfig = require('./actions/_getTemplateConfig')
const _prepare = require('./actions/_prepare')
const _merge = require('./actions/_merge')
const _exportToTif = require('./actions/_exportToTif')
const _makeSureOutput = require('./actions/_makeSureOutput');
const Promise = require('bluebird')
const _ = require('lodash')
const QRGenerator = require('./QRGenerator')
const os = require('os')
const osUtil = require('os-utils')

exports.print = async (items, options = {}) => {

    const {info, url} = options

    const qrType = info.type
    const qrs = await Promise.map(items, async item => {
        const content = `${url}/${item._id || ""}`

        return {input: await QRGenerator(content, qrType)}
    })

    const vItems = await _validateItems(qrs)

    const pages = _.chunk(vItems, qrType === 1 ? 200 : 50)

    console.log("LENGTH: ", pages.length)
    return Promise.map(pages, async (page, index) => {
        console.log('Running in page: ', index)

        const configItems = await _getTemplateConfig(page, qrType)
        const preparedItems = await Promise.map(page, async (item, index) => {
            const vConfig = configItems[index] || false

            return _prepare(item, vConfig)
        }, {concurrency: 4})

        const merged = await _merge(preparedItems, info, index)
        const {pathFile} = options
        const {qrIndex} = info
        await _makeSureOutput(pathFile);
        console.log(`total mem: ${os.totalmem()}, mem use: ${os.totalmem() - os.freemem()}`);
        console.log("FREEMEM: ---",os.freemem())
        osUtil.cpuUsage(function(v){
            console.log( 'CPU Usage (%): ' + v );
        });
        await _exportToTif(merged, pathFile + `/${qrIndex}.tif`)
        // return merged
    }, {concurrency: 3})

}