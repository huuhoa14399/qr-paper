const sharp = require('sharp');
const QRCode = require('qrcode');
const border = require('./actions/border');

const infoTemProduct = {
  qr: {
    width: 247,
    height: 247,
    top: 30,
    left: 335,
  },
  width: 613,
  height: 306
}

const infoTemPacking = {
  qr: {
    width: 572,
    height: 572,
    top: 20,
    left: 634,
  },
  width: 1226,
  height: 612
}

const generateQr = async (mainContent, qrType) => QRCode.toBuffer(mainContent, {
  width: qrType === 1 ? infoTemProduct.qr.width : infoTemPacking.qr.width,
  height: qrType === 1 ? infoTemProduct.qr.height : infoTemPacking.qr.height,
  type: 'png',
  margin: 0,
});

const generateInfo = async (mainContent, qrType, resizeWidth = null) => {
  const qr = await generateQr(mainContent, qrType);

  const buffer = await sharp({
    create: {
      width: qrType === 1 ? infoTemProduct.width : infoTemPacking.width,
      height: qrType === 1 ? infoTemProduct.height : infoTemPacking.height,
      channels: 4,
      background: {
        r: 255, g: 255, b: 255, alpha: 1,
      }, // White background
    },
  })
      .composite([
        {
          input: qr,
          top: qrType === 1 ? infoTemProduct.qr.top : infoTemPacking.qr.top,
          left: qrType === 1 ? infoTemProduct.qr.left : infoTemPacking.qr.left,
        },
      ])
      .png({ quality: 70 })
      .toBuffer();

  if (!resizeWidth || resizeWidth < 0) {
    return buffer;
  }

  return sharp(buffer)
      .resize({
        width: resizeWidth,
      })
      .png({ quality: 70, compressionLevel: 9, progressive: true })
      .toBuffer();
};

module.exports = async (mainContent, qrType = 1, resizeWidth = null) => {
  const buffer =  await generateInfo(mainContent, qrType, resizeWidth)

  return border(buffer, 1)
};
