interface Options {
    pathFile: String,
    info: Object,
    url: String
}

declare var QRPaper: {
    print(items: Array<Object>, options: Options): Promise<Buffer | String>
}

export default QRPaper

