const sharp = require('sharp')

const _border = 1

module.exports = async (width, height) => {

    const base = sharp({
        create: {
            width,
            height,
            channels: 4,
            background: {r: 0, g: 0, b: 0, alpha: 1}
        }
    })
        .limitInputPixels(0)
        .png({quality: 100})

    const insideW = Math.round(width - _border * 2)
    const insideH = Math.round(height - _border * 2)

    const inside = await sharp({
        create: {
            width: insideW,
            height: insideH,
            channels: 4,
            background: {r: 255, g: 255, b: 255, alpha: 1}
        }
    })
        .limitInputPixels(0)
        .png({quality: 100}).toBuffer()

    return base.composite([
        {
            input: inside,
            top: _border,
            left: _border,
            blend: 'dest-out'
        }
    ]).toBuffer()
}

