module.exports = (layoutWidth, total, widthPerItem) => {
    const space = layoutWidth - widthPerItem * total

    const padding = Math.floor(space / (total + 1))
    const maxPadding = 200

    return Math.min(maxPadding, padding)
}

