const _getItemsPerRow = require('./_getItemsPerRow')

module.exports = (totalItems = 8, width = 141, qrType = 1) => {
    const perItem = _getItemsPerRow(totalItems, qrType)
    console.log('perItem', perItem)

    return width * perItem
}