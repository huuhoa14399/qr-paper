module.exports = (total, itemsPerRow) => {
    return Math.ceil(total / itemsPerRow)
}

