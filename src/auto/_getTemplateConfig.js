const _getItemsPerRow = require('./_getItemsPerRow')
const getImageSize = require('../helpers/getImageSize')
const _getLayoutWidth = require('./_getLayoutWidth')
const _getRows = require('./_getRows')
const PADDING = 1
const PADDING_BOTTOM = 1

module.exports = async (items = [], qrType) => {
    const {length: totalItems} = items

    const {input} = items[0]
    const {width, height} = await getImageSize(input)

    const layoutWidth = _getLayoutWidth(totalItems, width, qrType)
    console.log('layoutWidth', layoutWidth)
    const perItems = _getItemsPerRow(totalItems, qrType)
    const numberRows = _getRows(totalItems, perItems)

    console.log('numberRows', numberRows)
    console.log('Padding', PADDING)
    console.log('paddingBottom', PADDING_BOTTOM)

    const vItems = []

    for (let row = 1; row <= numberRows; row++) {
        console.log('Row:', row)

        for (let column = 1; column <= perItems; column++) {
            console.log('Column:', column)

            let top = row === 1 ? 375 : Math.round((row - 1) * height + (row-1) * PADDING_BOTTOM) + 375
            const left = column === 1 ? 521 : Math.round((column - 1) * width + (column - 1) * PADDING) + 521
            const item = {
                location: {
                    top,
                    left,
                },
                PADDING
            }

            vItems.push(item)
        }
    }

    return vItems
}