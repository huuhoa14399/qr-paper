const _getItemsPerRow = require('./_getItemsPerRow')
const getImageSize = require('../helpers/getImageSize')
const _getLayoutWidth = require('./_getLayoutWidth')
const _getRows = require('./_getRows')
const _getPaddingBottom = require('./_getPaddingBottom')
const _createLayout = require('./_createLayout')


module.exports = async (totalItems = 4, items = []) => {
    const perItems = _getItemsPerRow(totalItems)
    const numberRows = _getRows(totalItems, perItems)
    const paddingBottom = 0
    const padding = 0

    console.log({paddingBottom})

    console.log({padding})
    const {input} = items[0]
    const {height, width} = await getImageSize(input)

    const layoutWidth = Math.round(perItems * width + (perItems + 1) * padding) + 51*2
    console.log('layoutWidth', layoutWidth)
    const layoutHeight = Math.round(numberRows * height + numberRows * paddingBottom) + 105
    console.log('layoutHeight', layoutHeight)

    console.log({layoutHeight, layoutWidth})
    return _createLayout(layoutWidth, layoutHeight)
}