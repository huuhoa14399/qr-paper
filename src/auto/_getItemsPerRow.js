const _mapsQRProduct = {
    1: 1,
    2: 2,
    3: 3,
    4: 4,
    5: 5,
    6: 6,
    7: 7,
    8: 8,
    default: (total) => {
        return 8
    }
}

const _mapsQRPacking = {
    1: 1,
    2: 2,
    3: 3,
    4: 4,
    default: (total) => {
        return 4
    }
}

module.exports = (total, qrType) => {

    if (qrType === 1) {
        return _mapsQRProduct[total] || _mapsQRProduct.default(total)
    }
    return _mapsQRPacking[total] || _mapsQRPacking.default(total)

}