const path = require('path')
const fs = require('fs-extra')
const sharp = require('sharp')

const _templatePath = path.join(__dirname, './assets/info.svg')

const _getTemplateContent = async () => {
    return await fs.readFile(_templatePath, {encoding: 'utf8'})
}

const _generateNumberText = async (number) => {
    const object = Object.assign({number})

    let template = await _getTemplateContent()

    for (let key in object) {
        if (object.hasOwnProperty(key)) {
            const value = object[key]
            const regex = new RegExp(`\{${key}\}`, 'gi')

            template = template.replace(regex, value)
        }
    }

    return Buffer.from(template)
}

const _generateNumber = async (mainContent, resizeWidth = null) => {
    const [svg] = await Promise.all([
        _generateNumberText(mainContent),
    ])

    const png = await sharp(svg)
        .limitInputPixels(0)
        .png({quality: 70})

    const {width, height} = await png.metadata()

    const buffer = await png.toBuffer()

    if (!resizeWidth || resizeWidth < 0) {
        return buffer
    }

    const image = await sharp(buffer)
        .limitInputPixels(0)
        .resize({width: resizeWidth})
        .png({quality: 70, compressionLevel: 9, progressive: true})
        .toBuffer()

    return await sharp({
        create: {
            width,
            height,
            channels: 4,
            background: {r: 255, g: 255, b: 255, alpha: 0.5}
        }
    })
        .limitInputPixels(0)
        .resize({width: resizeWidth})
        .png({quality: 70, compressionLevel: 9, progressive: true})
        .composite([
            {
                input: image,
                top: 0,
                left: 0,
            },
        ])
        .toBuffer()
}


module.exports = async (mainContent, resizeWidth = null) => {
    const buffer =  await _generateNumber(mainContent, resizeWidth)

    return sharp(buffer)
        .limitInputPixels(0)
        .toBuffer()
}
