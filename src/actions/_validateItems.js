module.exports = async (items = []) => {
    const vItems = Array.isArray(items) ? items : []

    return vItems.map(item => {
        const {input} = item

        if (!input) return false

        return {
            input,
        }
    }).filter(Boolean)
}