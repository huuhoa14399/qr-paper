const sharp = require('sharp')


module.exports = async (item, rotate) => {
    if (!rotate) return item

    return sharp(item)
        .limitInputPixels(0)
        .rotate(rotate)
        .png()
        .toBuffer()
}
