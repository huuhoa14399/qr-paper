module.exports = async (item, config) => {
    if (!config || typeof config !== 'object') {
        throw new Error('Config is empty or not setup.')
    }

    const {input} = item

    return {
        input,
        config,
    }
}