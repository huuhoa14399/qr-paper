const _getLayoutByTotal = require('../auto/_getLayoutByTotal')


module.exports = async (total = 2, items = []) => {
    return _getLayoutByTotal(total, items)
}
