const sharp = require('sharp')
const DPIToPPM = require('../helpers/DPIToPPM')

const DPI = 72

module.exports = async (buffer, pathFile) => {
    const pixelPerMini = DPIToPPM(DPI)

    console.log({pathFile})
    const info = await sharp(buffer)
        .limitInputPixels(0)
        .withMetadata()
        .tiff({
            quality: 70,
            xres: pixelPerMini,
            yres: pixelPerMini,
            compression: 'lzw'
        })
        .toFile(pathFile)

    console.log('Export to tiff:', info)

    return pathFile
}