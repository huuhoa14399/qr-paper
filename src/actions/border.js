const sharp = require('sharp');
const getImageSize = require('./getImageSize');

module.exports = async (image, borderWidth = 5) => {
  const { width, height } = await getImageSize(image);

  const border = sharp({
    create: {
      width: width + borderWidth * 2,
      height: height + borderWidth * 2,
      channels: 4,
      background: {
        r: 0, g: 0, b: 0, alpha: 1,
      }, // Black background
    },
  }).png({ quality: 70 });

  const background = await sharp({
    create: {
      width,
      height,
      channels: 4,
      background: {
        r: 255, g: 255, b: 255, alpha: 1,
      }, // White background
    },
  }).png({ quality: 70 }).toBuffer();

  return border.composite([
    {
      input: background,
      top: borderWidth,
      left: borderWidth,
      blend: 'dest-out',
    },
    {
      input: image,
      top: borderWidth,
      left: borderWidth,
    },
  ]).png({ quality: 70 })
    .toBuffer();
};
