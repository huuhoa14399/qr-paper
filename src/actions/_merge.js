const InfoGenerator = require('../InfoGenerator')
const sharp = require('sharp')
const WIDTH = 5953
const HEIGHT = 8419
const moment = require('moment')

const _setupComposite = (items) => {
    return items.map(item => {
        const {input, config} = item

        const {location} = config
        const {top, left} = location

        return {
            input,
            top,
            left,
        }
    })
}

const normalizeNumber = (number) => {
    let str = number.toString()
    let pad = '00'
    return pad.substring(0, pad.length - str.length) + str
}

module.exports = async (items = [], info, index) => {
    const {length: total} = items

    if (!total) {
        throw new Error("Items are empty.")
    }

    const cItems = _setupComposite(items)

    const {creator, qrCode, qrIndex, type} = info

    const creatorBuffer = await InfoGenerator(creator)
    cItems.push({
        input: creatorBuffer,
        top: 105,
        left: 521,
    })

    const today = moment().format("DD/MM/YYYY")
    const hours = new Date().getHours()
    const minutes = new Date().getMinutes()
    const seconds = new Date().getSeconds()

    const creatorAt = `${hours}: ${minutes}: ${seconds} - ${today}`
    const creatorArBuffer = await InfoGenerator(creatorAt)
    cItems.push({
        input: creatorArBuffer,
        top: 175,
        left: 521,
    })
    const position = `VNC: ${qrCode}.${normalizeNumber(qrIndex)}`
    const positionArBuffer = await InfoGenerator(position)
    cItems.push({
        input: positionArBuffer,
        top: 225,
        left: 521,
    })

    return sharp({
        create: {
            width: WIDTH,
            height: HEIGHT,
            channels: 4,
            background: {r: 255, g: 255, b: 255, alpha: 1}
        }
    })
        .composite(cItems)
        .png({quality: 70, compressionLevel: 9, progressive: true})
        .toBuffer()
}