const getImageSize = require('../helpers/getImageSize')
const sharp = require('sharp')
const WIDTH = 794
const HEIGHT = 1123

module.exports = async (layout) => {
    const {width, height} = await getImageSize(layout)

    return sharp({
        create: {
            width: WIDTH,
            height: HEIGHT,
            channels: 4,
            background: {r: 255, g: 255, b: 255, alpha: 1}
        }
    })
        .limitInputPixels(0)
        .png({quality: 100})
}

