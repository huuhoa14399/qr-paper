const fs = require('fs-extra')
const path = require('path')
const _getTemplateConfig = require('../auto/_getTemplateConfig')
const _isAuto = require('./_isAuto')


module.exports = async (items, qrType) => {
    if (!items || !items.length) {
        throw new Error('Items are empty or invalid.')
    }

    const total = items.length

    console.log({total})

    return _getTemplateConfig(items, qrType)

}