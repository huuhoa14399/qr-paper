const svgb = require('svgb')
const QRCode = require('qrcode')
const path = require('path')
const DPIToPPM = require('../helpers/DPIToPPM')
const sharp = require('sharp')
const getImageSize = require('../helpers/getImageSize')

const template = path.join(__dirname, '../assets/batch-info.svg')
const DPI = 150
const QR_SIZE_CM = 2 // centimeters
const LABEL_LEVEL_SIZE = 200 // pixels

const _exportToTif = async (buffer, pathFile) => {
    const pixelPerMini = DPIToPPM(DPI)

    return sharp(buffer)
        .limitInputPixels(0)
        .flop(true)
        .tiff({
            quality: 100,
            xres: pixelPerMini,
            yres: pixelPerMini,
            compression: 'lzw'
        })
        .toFile(pathFile)
}

const _mapLevels = {
    'MEDIUM': '#e5e2e2',
    'HIGHEST': '#ff2500',
    'HIGH': '#ffdb00',
}

const _mapLabels = {
    'MEDIUM': 'BÌNH THƯỜNG',
    'HIGHEST': 'RẤT GẤP',
    'HIGH': 'GẤP',
}

const _hexToRgb = (hex) => {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)

    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null
}

const _getPriorityLabelV1 = (level) => {
    const color = _mapLevels[level] || _mapLevels['MEDIUM']

    const rgb = _hexToRgb(color)

    return sharp({
        create: {
            width: LABEL_LEVEL_SIZE,
            height: LABEL_LEVEL_SIZE,
            channels: 4,
            background: Object.assign({r: 255, g: 255, b: 255, alpha: 1}, rgb),
        }
    })
        .limitInputPixels(0)
        .png().toBuffer()
}

const _cmToPxWithDPI = (cm, dpi = DPI) => {
    return Math.floor(cm * dpi / 2.54)
}

const _getQRCode = (batchName) => {
    return QRCode.toBuffer(batchName, {
        width: _cmToPxWithDPI(QR_SIZE_CM),
        type: 'png',
        margin: 0,
    })
}

const _generateBatchInfo = async (info, pathFile = '') => {
    if (!pathFile) throw new Error('Path file output is required.')

    const {
        name,
        level,
        created_at,
        total_orders,
        total_files,
        note,
        product_types,
        user_create,
    } = info

    const vProductTypes = Array.isArray(product_types) ? product_types : []
    const total = `${total_orders} / ${total_files}`

    const vLevel = (level || 'medium').toString().trim().toUpperCase()

    const vArgs = {
        name: (name || '').toString().trim().toUpperCase(),
        level: _mapLabels[vLevel] || _mapLabels['MEDIUM'],
        level_color: _mapLevels[vLevel] || _mapLevels['MEDIUM'],
        user_create: (user_create || '').toString().trim(),
        created_at: (created_at || '').toString().trim(),
        total: (total || '').toString().trim(),
        note: (note || '').trim(),
        product_types: vProductTypes.join(', ')
    }

    // const templateBuffer = await svgb.toBuffer(template, vArgs)

    const [templateBuffer, batchQRCode] = await Promise.all([svgb.toBuffer(template, vArgs), _getQRCode(vArgs.name)])
    const {width: templateWidth, height: templateHeight} = await getImageSize(templateBuffer)

    const QRSize = _cmToPxWithDPI(QR_SIZE_CM)

    const buffer = await sharp(templateBuffer)
        .limitInputPixels(0)
        .composite([
        {
            input: batchQRCode,
            top: 20,
            left: templateWidth - QRSize - 20,
        }
    ])
        .png({quality: 100})
        .toBuffer()

    return _exportToTif(buffer, pathFile)
}

module.exports = _generateBatchInfo
