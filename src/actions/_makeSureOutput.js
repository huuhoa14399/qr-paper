const mkdirp = require('mkdirp')


module.exports = async (dir) => {
    await mkdirp(dir)

    return true
}
