const fs = require('fs-extra')
const path = require('path')


module.exports = async (pathInfo) => {
    if (!pathInfo) {
        return ""
    }

    const file = path.join(pathInfo)

    try {
        const content = await fs.readFile(file, {encoding: 'utf8'})
        const object = JSON.parse(content)

        const {name} = name

        return (name ? name : "").trim()
    } catch (e) {
        console.log('ERROR', e)

        throw new Error('Read info config failed.')
    }
}