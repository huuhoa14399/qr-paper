const _manualTemplates = []


module.exports = (totalItems = 0, type) => {

    if (type === "MASKCC_3D" || type === "MASKCCNUT_3D" || type === "MASK_PS_3D" || type === "MASK_PQ_3D" || type === 'MASKCC_V2_3D') {
        return true
    }
    return !_manualTemplates.includes(totalItems)
}

