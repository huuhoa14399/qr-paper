const moment = require('moment')
setImmediate(async () => {
    const hours = new Date().getHours()
    const minutes = new Date().getMinutes()
    const seconds = new Date().getSeconds()

    const today = moment().format("DD/MM/YYYY")
    console.log({today})
    console.log(`${hours}: ${minutes}: ${seconds}`)
})
