const path = require('path')
const QRPaper = require('../index')
const qr = path.join(__dirname, 'QR.png')
const QRGenerator = require('../src/QRGenerator')
const Promise = require('bluebird')
const run = async (items, index) => {

    const out = path.join(__dirname, `${index}/output`)
    return await QRPaper.print(items, {pathFile: out, info: {
            creator: "Người tạo mã: Admin VnCheck", creatorAt: "Ngày tạo mã: 26/12/2020", type: "Loại mã: Đóng gói"
        }, url: "http://13.251.125.228:9001/"})

}
setImmediate(async () => {
    try {
        // let items = []
        // for (let i = 0; i < 200; i ++) {
        //     items.push({
        //         "scanLocation": {
        //             "coordinates": []
        //         },
        //         "children": [],
        //         "_id": "5ff48a2513c9e60051aeb3aa",
        //         "createdBy": "5ff489a6c41d5d0034544eaa",
        //         "type": "1",
        //         "enterprise": {
        //             "name": "UniFarm",
        //             "taxId": "0123456789",
        //             "address": "123 Trung Kính, Cầu Giấy",
        //             "phone": "0961782317",
        //             "presentedBy": "Nguyễn Văn A",
        //             "gln": "123456"
        //         },
        //         "code": "622259cb7f",
        //         "__v": 0
        //     })
        // }
        // const out = path.join(__dirname, `/output`)
        //
        // await QRPaper.print(items, {pathFile: out, info: {
        //         creator: "Người tạo mã: Admin VnCheck", creatorAt: "dd/mm/yyy: 07/03/2021", qrCode: "000000001", type: 2,
        //     }, url: "http://13.251.125.228:9001/"})

        const normalizeNumber = (number) => {
            let str = number.toString()
            let pad = '00'
            return pad.substring(0, pad.length - str.length) + str
        }

        const buffers = await Promise.each(Array(50), async (_empty, index) => {
            let results = []
            for (let i = 0; i < 200; i ++) {
                results.push({
                    "scanLocation": {
                        "coordinates": []
                    },
                    "children": [],
                    "_id": "5ff48a2513c9e60051aeb3aa",
                    "createdBy": "5ff489a6c41d5d0034544eaa",
                    "type": "1",
                    "enterprise": {
                        "name": "UniFarm",
                        "taxId": "0123456789",
                        "address": "123 Trung Kính, Cầu Giấy",
                        "phone": "0961782317",
                        "presentedBy": "Nguyễn Văn A",
                        "gln": "123456"
                    },
                    "code": "622259cb7f",
                    "__v": 0
                })
            }
            const out = path.join(__dirname, `/output/qrcode`)

            return (QRPaper.print(results.filter(Boolean), {
                pathFile: out,
                info: {
                    creator: "Người tạo mã: Admin VnCheck", creatorAt: "dd/mm/yyy: 07/03/2021", qrCode: "000000001", qrIndex: `${normalizeNumber(index + 1)}`, type: 1,
                }, url: "http://13.251.125.228:9001/"
            }));
        });
        // const tmpArray = []
        // const startTime = new Date().getTime()
        // for (let i = 0; i < 5; i++) {
        //     tmpArray.push(i)
        // }
        //
        //
        // await Promise.map(tmpArray, async (item, index) => {
        //     console.log("running index: ", index)
        //     await run(items, index)
        // }, {concurrency: 10})
        // const endTime = new Date().getTime() - startTime
        //
        // console.log({startTime, endTime})

        // const result = await Promise.map(items, async item => {
        //     const content = {
        //         _id: item._id,
        //         code: item.code,
        //         type: item.type,
        //         createdBy: item.createdBy,
        //     };
        //
        //     return {input: await QRGenerator(JSON.stringify(content))}
        // })
        //
        // console.log({result})
        // const out = path.join(__dirname, 'output')
        // console.log({out})
        // const x = await QRPaper.print(items, {pathFile: out, info: {
        //     creator: "Người tạo mã: Admin VnCheck", creatorAt: "Ngày tạo mã: 26/12/2020", type: "Loại mã: Đóng gói"
        //     }, url: "http://13.251.125.228:9001/"})
        //
        // console.log('RESULT:', x)
    } catch (e) {
        console.log("ERROR:", e)
    }
})