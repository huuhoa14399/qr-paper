const PDFDocument = require('pdfkit')
const fs = require('fs')
const _getImageSize = require('../src/helpers/getImageSize')
const path = require('path')
const imagesToPdf = require("images-to-pdf")
const globby = require('globby')
setImmediate(async () => {

    // const {width, height} = await _getImageSize(imagePath)
    // const doc = new PDFDocument

    // console.log({width, height})
    // doc.pipe(fs.createWriteStream('output.pdf'))
    //
    // doc.addPage()
    //     .image(imagePath, {
    //         fit: [width,height],
    //     });
    //
    // doc.end()

    const dir = path.join(__dirname, "/output/qrcode")
    console.log({dir})
    const images = await globby([
        dir + '/*.tif',
    ], {onlyFiles: true})
    await imagesToPdf(images, `./output/anhthinh.pdf`)


})
