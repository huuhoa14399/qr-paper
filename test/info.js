const {generateBatchInfo} = require('../index')
const path = require('path')


setImmediate(async () => {
    const out = path.join(__dirname, 'info.tif')

    try {
        await generateBatchInfo({
            name: 'B-4533333',
            created_at: '20/20/2020',
            total_orders: 2222,
            total_files: 2222,
            note: 'HEllo abc',
            user_create: 'Tu',
            product_types: ['MASK_3D', 'MASK-3D']
        }, out)
    } catch (e) {
        console.log("ERROR", e)
    }
})
