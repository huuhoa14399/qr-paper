const sharp = require('sharp')
const fs = require('fs')
const path = require('path')
setImmediate(async () => {
    const image = path.join(__dirname, 'output/1.tif')
    const abc = await sharp(image)
        .tiff({
            compression: 'jpeg',
            bitdepth: 1,
            quality: 10
        })
        .toFile('1-bpp-output.tiff')
    console.log(abc)
})
